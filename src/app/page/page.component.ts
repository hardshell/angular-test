import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { DataItem } from '../dataInterface'

@Component({
  selector: 'app-page1',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  providers: [ DataService ]
})

export class PageComponent implements OnInit {
  data: DataItem[];

  constructor(private dataService: DataService){}

  ngOnInit() {
    this.dataService.fetchData().subscribe((data: DataItem[]) => this.data = data)
  }
}
