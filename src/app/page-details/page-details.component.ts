import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { DataItem } from '../dataInterface'

@Component({
  selector: 'app-page-details',
  templateUrl: './page-details.component.html',
  styleUrls: ['./page-details.component.scss'],
  providers: [ DataService ]
})

export class PageDetailsComponent implements OnInit{
  data: DataItem[];
  id: DataItem['id'];
  name: DataItem['name'];

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService){
    this.id = activatedRoute.snapshot.params['id'];
  }

  filterData() {
    this.data.filter((el: DataItem) => {
      if(this.id == el.id) {
        this.name = el.name;
      }
    })
  }

  ngOnInit() {
    this.dataService.fetchData().subscribe((data: DataItem[]) => {
      this.data = data;
      this.filterData();
    })
  }
}