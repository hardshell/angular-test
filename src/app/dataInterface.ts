export interface DataItem {
  id: Number;
  name: String;
  rating: Number;
}