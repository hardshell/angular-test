import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { ModuleValuePipe } from './moduleValue.pipe';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { PageDetailsComponent } from './page-details/page-details.component';
import { PageComponent } from './page/page.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DataService } from './data.service';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'page1', component:  PageComponent },
  { path: 'page2', component:  PageComponent },
  { path: 'page1/:id', component: PageDetailsComponent },
  { path: 'page2/:id', component: PageDetailsComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageDetailsComponent,
    PageComponent,
    HomeComponent,
    NotFoundComponent,
    ModuleValuePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
