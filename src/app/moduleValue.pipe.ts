import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moduleValue'
})

export class ModuleValuePipe implements PipeTransform {
  transform(value: number) : number {
    return Math.abs(value)
  }
}